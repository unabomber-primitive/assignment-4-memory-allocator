#ifndef _TESTS_H_
#define _TESTS_H_

#include <stdio.h>

void test_success();
void test_one_free();
void test_two_free();
void test_extend();
void test_unable_to_extend();

#define RUN_TEST(name)  {                       \
    printf("RUNNING TEST " #name "\n");         \
    test_##name();                              \
    printf("TEST " #name ": SUCCESS!\n\n");     \
}

inline void run_tests() {
    RUN_TEST(success);
    RUN_TEST(one_free);
    RUN_TEST(two_free);
    RUN_TEST(extend);
    RUN_TEST(unable_to_extend);
}

#endif